<?php

namespace App\Http\Middleware;

use Closure;
use App\Lib\Exceptions\MiddlewareException;
use App\Model\api\UserToken;
use Auth;

class UserCheck
{
    /**
     * Handle an incoming request.
     *
     * @security [
     *     {
     *          "name": "Authorization",
     *          "in": "header",
     *          "type": "apiKey"
     *     }
     * ]
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user          = null;
        $authorization = !empty($request->header('Authorization')) ? trim($request->header('Authorization')) : '';
        
        if (!empty($authorization) && strpos($authorization, 'Bearer ') !== false) {
            try {
                $list      = explode(" ", $request->header('Authorization'));
                $tokenUser = $list[1] ?? null;

                $userToken = UserToken::token($tokenUser)->first();
                
                if (!empty($userToken) && $userToken->exists) {
                    if (empty($userToken->updated) || strtotime($userToken->updated) <= strtotime(date("Y-m-d"))) {
                        $userToken->updated = date("Y-m-d");
                        $userToken->save();
                    }

                    $user = $userToken->users;
                    $user->setToken($tokenUser);
                }
            } catch (\Exception $e) {
                throw new MiddlewareException('Token error');
            }
        }

        if (empty($user)) {
            throw new MiddlewareException('Not authenticated');
        }

        Auth::setUser($user);
        
        return $next($request);
    }
}
