<?php

namespace App\GraphQL\internal\Query;

use App\Model\api\Events as EventsModel;
use App\GraphQL\FieldType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class Events extends Query
{
    protected $attributes = [
        'name' => 'Events query'
    ];

    public function type(): Type
    {
        return FieldType::listOf(FieldType::type('events'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => FieldType::int()
            ],
        ];
    }

    public function resolve($args)
    {
        if (!empty($args['id'])) {
            return EventsModel::find($args['id']);
        }

        return EventsModel::all();
    }
}
