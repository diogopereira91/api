<?php

namespace App\GraphQL\internal\Query;

use App\Model\api\User;
use App\GraphQL\FieldType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class Users extends Query
{
    protected $attributes = [
        'name' => 'Users query'
    ];

    public function type(): Type
    {
        return FieldType::listOf(FieldType::type('user'));
    }

    public function args(): array
    {
        return [
            'id' => [
                'name' => 'id',
                 'type' => FieldType::int()
            ],
            'email' => [
                'name' => 'email',
                 'type' => FieldType::string()
            ],
        ];
    }

    public function resolve($args)
    {
        if (isset($args['id'])) {
            return User::where('id' , $args['id'])->get();
        }

        if (isset($args['email'])) {
            return User::where('email', $args['email'])->get();
        }

        return User::all();
    }
}
