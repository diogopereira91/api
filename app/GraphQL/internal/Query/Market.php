<?php

namespace App\GraphQL\internal\Query;

use App\Model\api\Market as MarketModel;
use App\GraphQL\FieldType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class Market extends Query
{
    protected $attributes = [
        'name' => 'Market query'
    ];

    public function type(): Type
    {
        return FieldType::listOf(FieldType::type('market'));
    }

    public function args(): array
    {
        return [
            'userId' => [
                'name' => 'id',
                'type' => FieldType::int()
            ],
        ];
    }

    public function resolve($args)
    {
        if (!empty($args['userId'])) {
            return MarketModel::where('userId', $args['userId'])->get();
        }

        return MarketModel::all();
    }
}
