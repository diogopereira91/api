<?php

namespace App\GraphQL\internal\Query;

use App\Model\api\Documents as DocumentModel;
use App\GraphQL\FieldType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;

class Documents extends Query
{
    protected $attributes = [
        'name' => 'Documents query'
    ];

    public function type(): Type
    {
        return FieldType::listOf(FieldType::type('documents'));
    }

    public function resolve()
    {
        return DocumentModel::all();
    }
}
