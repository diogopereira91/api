<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class User extends GraphQLType
{
    protected $attributes = [
        'name'          => 'User',
        'description'   => 'A user',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::nonNull(FieldType::string()),
                'description' => 'The id of the user',
            ],
            'email' => [
                'type' =>  FieldType::nonNull(FieldType::string()),
                'description' => 'The email of user',
            ],
            'userType' => [
                'type' => FieldType::type('usertype'),
                'description' => 'User Type'
            ],
            'profile' => [
                'type' => FieldType::type('profile'),
                'description' => 'User profile',
            ],
        ];
    }
}
