<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class MarketComments extends GraphQLType
{
    protected $attributes = [
        'name'          => 'MarketComments',
        'description'   => 'MarketComments type',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::nonNull(FieldType::string()),
                'description' => 'Comment id',
            ],
            'comment' => [
                'type' =>  FieldType::string(),
                'description' => 'Comment content',
            ],
            'market' => [
                'type' => FieldType::type('market'),
                'description' => 'Market identification'
            ],
            'users' => [
                'type' => FieldType::type('user'),
                'description' => 'User that commented',
            ],
            'created' => [
                'type' => FieldType::string(),
                'description' => 'Date of comment creation',
            ],
        ];
    }
}
