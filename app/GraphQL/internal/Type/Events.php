<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Events extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Events',
        'description'   => 'Events type',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::nonNull(FieldType::string()),
                'description' => 'Event id',
            ],
            'users' => [
                'type' =>  FieldType::type('user'),
                'description' => 'Event responsible',
            ],
            'description' => [
                'type' => FieldType::string(),
                'description' => 'Event description'
            ],
            'rules' => [
                'type' => FieldType::string(),
                'description' => 'Event rules, hours and other information',
            ],
            'eventDate' => [
                'type' => FieldType::string(),
                'description' => 'Event predicted date',
            ],
            'picture' => [
                'type' => FieldType::string(),
                'description' => 'Event picture',
                'resolve' => function($root) {
                    return env('APP_URL') . '/' . $root->picture;
                },
            ],
            'fields' => [
                'type' => FieldType::type('fields'),
                'description' => 'Field information',
            ],
            // 'eventsHasUsers' => [
            //     'type' => FieldType::type('events_has_users'),
            //     'description' => 'Users registered to this event'
            // ],
        ];
    }
}
