<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class UserType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'UserTypes',
        'description'   => 'A user',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::int(),
                'description' => 'The id of the user',
            ],
            'description' => [
                'type' => FieldType::string(),
                'description' => 'The email of user',
            ],
        ];
    }
}
