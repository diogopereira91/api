<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Market extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Market',
        'description'   => 'Market type',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::nonNull(FieldType::string()),
                'description' => 'Item id',
            ],
            'itemTitle' => [
                'type' =>  FieldType::string(),
                'description' => 'Item title',
            ],
            'itemDescription' => [
                'type' =>  FieldType::string(),
                'description' => 'Description of the item',
            ],
            'itemPrice' => [
                'type' =>  FieldType::float(),
                'description' => 'Description of the item',
            ],
            'visible' => [
                'type' => FieldType::boolean(),
                'description' => 'is/is not visible',
            ],
            'itemSoldAt' => [
                'type' => FieldType::string(),
                'description' => 'Date than the item was sold',
            ],
            'users' => [
                'type' => FieldType::type('user'),
                'description' => 'Publication owner',
            ],
            'comments' => [
                'type' => FieldType::listOf(FieldType::type('market_comments')),
                'description' => 'Publication comments'
            ],
            'itemType' => [
                'type' => FieldType::type('item_type'),
                'description' => 'Item type',
            ],
            'created' => [
                'type' => FieldType::string(),
                'description' => 'Date of creation',
            ],
        ];
    }
}
