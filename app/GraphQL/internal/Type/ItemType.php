<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ItemType extends GraphQLType
{
    protected $attributes = [
        'name'          => 'ItemType',
        'description'   => 'ItemType type',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::nonNull(FieldType::string()),
                'description' => 'ItemType id',
            ],
            'description' => [
                'type' =>  FieldType::string(),
                'description' => 'ItemType name',
            ],
        ];
    }
}
