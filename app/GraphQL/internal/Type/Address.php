<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Address extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Address',
        'description'   => 'Address type',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::nonNull(FieldType::string()),
                'description' => 'The id of the user',
            ],
            'street' => [
                'type' =>  FieldType::string(),
                'description' => 'The email of user',
            ],
            'doorNumber' => [
                'type' => FieldType::string(),
                'description' => 'User Type'
            ],
            'zipCode' => [
                'type' => FieldType::string(),
                'description' => 'User profile',
            ],
            'parish' => [
                'type' => FieldType::type('parish'),
                'description' => 'Parish id'
            ]
        ];
    }
}
