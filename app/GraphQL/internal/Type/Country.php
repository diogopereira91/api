<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Country extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Country',
        'description'   => 'Country type',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::nonNull(FieldType::string()),
                'description' => 'Country id',
            ],
            'description' => [
                'type' =>  FieldType::string(),
                'description' => 'Country name',
            ],
            'code' => [
                'type' => FieldType::string(),
                'description' => 'country code'
            ],
        ];
    }
}
