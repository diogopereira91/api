<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Profile extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Profile',
        'description'   => 'User Profile',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::string(),
                'description' => 'The id of the user',
            ],
            'name' => [
                'type' => FieldType::string(),
                'description' => 'Users name',
            ],
            'idNumber' => [
                'type' => FieldType::string(),
                'description' => 'User identification number',
            ],
            'socialNumber' => [
                'type' => FieldType::string(),
                'description' => 'User fiscal number',
            ],
            'address' => [
                'type' => FieldType::type('address'),
                'description' => 'Users Address'
            ],
            'association' => [
                'type' => FieldType::type('association'),
                'description' => 'Users association information',
            ],
            'associationExpiration' => [
                'type' => FieldType::string(),
                'description' => 'Association expiration date',
            ],
            // 'quotesExpitation' => [
            //     'type' => FieldType::string(),
            //     'description' => 'KARAM quotes expiration date',
            // ],
            // 'created' => [
            //     'type' => FieldType::date([
            //         'description' => 'Member since',
            //     ]),
            // ],
        ];
    }
}
