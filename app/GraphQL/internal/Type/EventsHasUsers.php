<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class EventsHasUsers extends GraphQLType
{
    protected $attributes = [
        'name'          => 'EventsHasUsers',
        'description'   => 'EventsHasUsers type',
    ];

    public function fields(): array
    {
        return [
            'eventsId' => [
                'type' => FieldType::int('id'),
                'description' => 'EventsId',
            ],
            'users' => [
                'type' =>  FieldType::type('user'),
                'description' => 'User identification',
            ],
            'created' => [
                'type' => FieldType::string(),
                'description' => 'Date of registry'
            ],
        ];
    }
}
