<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Parish extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Parish',
        'description'   => 'Parish type',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::nonNull(FieldType::string()),
                'description' => 'Parish id',
            ],
            'description' => [
                'type' =>  FieldType::string(),
                'description' => 'Parish name',
            ],
            'county' => [
                'type' => FieldType::type('county'),
                'description' => 'County type'
            ],
        ];
    }
}
