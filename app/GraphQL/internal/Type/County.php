<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class County extends GraphQLType
{
    protected $attributes = [
        'name'          => 'County',
        'description'   => 'County type',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::nonNull(FieldType::string()),
                'description' => 'County id',
            ],
            'description' => [
                'type' =>  FieldType::string(),
                'description' => 'County name',
            ],
            'country' => [
                'type' => FieldType::type('country'),
                'description' => 'Country type'
            ],
        ];
    }
}
