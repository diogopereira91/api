<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Association extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Association',
        'description'   => 'Association type',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::nonNull(FieldType::string()),
                'description' => 'Association id',
            ],
            'name' => [
                'type' =>  FieldType::string(),
                'description' => 'Association name',
            ],
            'code' => [
                'type' => FieldType::string(),
                'description' => 'Association code'
            ],
        ];
    }
}
