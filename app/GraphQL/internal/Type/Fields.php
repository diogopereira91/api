<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Fields extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Fields',
        'description'   => 'Fields type',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::nonNull(FieldType::string()),
                'description' => 'The id of the field',
            ],
            'name' => [
                'type' =>  FieldType::string(),
                'description' => 'fieldName',
            ],
            'picture' => [
                'type' => FieldType::string(),
                'description' => 'Event picture',
                'resolve' => function($root) {
                    return env('APP_URL') . '/' . $root->picture;
                },
            ],
            'parish' => [
                'type' => FieldType::type('parish'),
                'description' => 'Field Localization',
            ],
        ];
    }
}
