<?php
namespace App\GraphQL\internal\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Documents extends GraphQLType
{
    protected $attributes = [
        'name'          => 'Documents',
        'description'   => 'Documents type',
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => FieldType::nonNull(FieldType::string()),
                'description' => 'Document id',
            ],
            'description' => [
                'type' =>  FieldType::string(),
                'description' => 'Document name',
            ],
            'url' => [
                'type' => FieldType::string(),
                'description' => 'Url path to document',
                'resolve' => function($root) {
                    return env('APP_URL') . '/' . $root->url;
                }
            ],
            'users' => [
                'type' => FieldType::type('user'),
                'description' => 'User that posted it',
            ],
            'created' => [
                'type' => FieldType::string(),
                'description' => 'Date of creation',
            ],
        ];
    }
}
