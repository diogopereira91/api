<?php

namespace App\GraphQL\external\Query;

use App\Facades\GraphQLErrors;
use App\Model\api\User;
use App\GraphQL\FieldType;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use Auth;

class Login extends Query
{
    protected $attributes = [
        'name' => 'Login Query'
    ];

    public function type(): Type
    {
        return FieldType::type('login');
    }

    public function args(): array
    {
        return [
            'email' => [
                'name' => 'email',
                'type' => FieldType::nonNull(FieldType::string()),
            ],
            'password' => [
                'name' => 'password',
                 'type' => FieldType::nonNull(FieldType::string()),
            ],
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $result          = new \stdClass();
        $result->success = false;

        $user = User::select()
                        ->email($args['email'])
                        ->whereNull('users.deleted')
                        ->first();

        if (!empty($user) && $user->validPassword($args['password'])) {

            $result->user    = $user;
            $result->success = true;

            $outputFields = $info->getFieldSelection(10);
            if (!empty($outputFields['token'])) {
                $result->token = $user->getToken();
            }
            
            Auth::setUser($user);

            return $result;
        }
        throw GraphQLErrors::get('INVALID_LOGIN');
    }
}
