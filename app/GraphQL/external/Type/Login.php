<?php
namespace App\GraphQL\external\Type;

use App\GraphQL\FieldType;
use Rebing\GraphQL\Support\Type as GraphQLType;

class Login extends GraphQLType
{
    protected $attributes = [
        'name'          => 'login',
        'description'   => 'Authentitacion response',
    ];

    public function fields(): array
    {
        return [
            'token' => [
                'type' => FieldType::string(),
                'description' => 'Authentication token',
            ],
            'success' => [
                'type' => FieldType::boolean(),
                'description' => 'Authentication success',
            ],
            'user' => [
                'type' => FieldType::type('user'),
                'description' => 'Authenticated user',
            ]
        ];
    }
}
