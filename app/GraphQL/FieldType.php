<?php
namespace App\GraphQL;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;

class FieldType
{

    /**
     * Returns string type
     */
    public static function string()
    {
        return Type::string();
    }


    /**
     * Returns boolean type
     */
    public static function boolean()
    {
        return Type::boolean();
    }

    /**
     * Returns id type
     */
    public static function id()
    {
        return Type::id();
    }

    /**
     * Returns int type
     */
    public static function int()
    {
        return Type::int();
    }

    /**
     * Returns float type
     */
    public static function float()
    {
        return Type::float();
    }

    /**
     * Returns listOf type
     */
    public static function listOf($listType)
    {
        return Type::listOf($listType);
    }

    /**
     * Returns nonNull type
     */
    public static function nonNull($listType)
    {
        return Type::nonNull($listType);
    }

    /**
     * Returns defined type
     */
    public static function type($name)
    {
        return GraphQL::Type($name);
    }

    /**
     * Returns defined sublist
     */
    public static function sublist($name)
    {
        return GraphQL::sublist($name);
    }

    /**
     * Returns date type
     */
    public static function date(array $conf = []): array
    {
        $field    = $conf['field'] ?? null;
        $resolve  = $conf['resolve'] ?? null;
        $dateTime = $conf['dateTime'] ?? true;

        $result = [
            'type'        => Type::string(),
            'description' => 'Date field',
            'args'        => [
                'dateFormat' => [
                    'name'         => 'dateFormat',
                    'type'         => Type::string(),
                    'defaultValue' => $dateTime ? "Y-m-d H:i:s" : "Y-m-d",
                ],
            ],
            'resolve'     => function ($root, $args, $context, ResolveInfo $info) use ($field, $resolve) {
                $field      = $field ?? $info->fieldName;
                $dateFormat = !empty($args['dateFormat']) ? $args['dateFormat'] : 'Y-m-d H:i:s';
                if (empty($resolve)) {
                    $time = $root->$field ?? $root[$field] ?? null;
                } else {
                    $time = $resolve($root, $args);
                }

                return !empty($time) ? date($dateFormat, strtotime($time)) : null;
            },
        ];

        if (!empty($conf['name'])) {
            $result['name'] = $conf['name'];
        }

        if (!empty($conf['description'])) {
            $result['description'] = $conf['description'];
        }

        unset($conf['field']);
        unset($conf['resolve']);

        return array_merge($result, $conf);
    }
}
