<?php
namespace App\Facades;

use Exception;

class GraphQLErrors extends Exception
{
    public static $errors = [
        'INVALID_LOGIN' => ['message' => 'Wrong login credentials', 'mensagem' => 'As suas credenciais estão incorretas'],
    ];

    /**
     * Returns a exception message to specific code
     *
     * @param string $key
     * @return Exception
     */
    public static function get(string $key)
    {
        
        if (array_key_exists($key, self::$errors)) {
            $error = self::$errors[$key];
            
            return new GraphQLErrors($key);
        } else {
            return new \Exception("GraphQL custom error code undefined");
        }
    }
}