<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class Association extends Model
{
    protected $table = "association";

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `association` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(100) NOT NULL,
        `code` varchar(45) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";


    /**
     * Relationship with profile table
     */
    public function profile()
    {
        return $this->hasMany('App\Model\api\Profile');
    }
}
