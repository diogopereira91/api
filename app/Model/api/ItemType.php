<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
    protected $table = "item_type";

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `item_type` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `description` varchar(150) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";


    /**
     * Relationship with market table
     */
    public function market()
    {
        return $this->hasMany('App\Model\api\Market');
    }
}
