<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;
use App\Model\Scopes\Active;

class UserType extends Model
{
    protected $table = 'users_type';

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `users_type` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `description` varchar(45) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

    const ADMIN = 1;
    const MOD = 2;
    const USER = 3;
    const INACTIVE = 4;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new Active);
    }

    /**
     * Relationship with user table
     */
    public function users()
    {
        return $this->hasMany('App\Model\api\User');
    }
}
