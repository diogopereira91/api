<?php

namespace App\Model\api;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Model\Scopes\Active;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Set database table name
     */
    protected $table = 'users';

    /**
     * Disabling automatic timestamps
     */
    public $timestamps = false;

    protected $DDL = "CREATE TABLE `users` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `email` varchar(150) NOT NULL,
        `password` varchar(255) NOT NULL,
        `usersTypeId` int(11) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_users_users_type_idx` (`usersTypeId`),
        CONSTRAINT `fk_users_users_type` FOREIGN KEY (`usersTypeId`) REFERENCES `users_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new Active);
    }

    /**
     * Relationship with profile table
     */
    public function profile()
    {
        return $this->hasOne('App\Model\api\Profile', 'usersId');
    }

    /**
     * Relationship with user_type table
     */
    public function userType()
    {
        return $this->belongsTo('App\Model\api\UserType', 'usersTypeId');
    }

    /**
     * Relatioship with user_token table
     */
    public function userToken()
    {
        return $this->hasMany('App\Model\api\UserToken', 'userId');
    }

    /**
     * Relationship with password_resets table
     */
    public function passwordResets()
    {
        return $this->hasMany('App\Model\api\PasswordResets', 'id', 'usersId');
    }

    /**
     * Relationship with news table
     */
    public function news()
    {
        return $this->hasMany('App\Model\api\News', 'id', 'usersId');
    }

    /**
     * Relationship with news comments table
     */
    public function comments()
    {
        return $this->hasMany('App\Model\api\NewsComments', 'id', 'usersId');
    }

    /**
     * Relationship with documents table
     */
    public function documents()
    {
        return $this->hasMany('App\Model\api\Documents', 'id', 'usersId');
    }

    /**
     * Relationship with market table
     */
    public function market()
    {
        return $this->hasMany('App\Model\api\Market');
    }

    /**
     * Relationship with market comments table
     */
    public function marketComments()
    {
        return $this->hasMany('App\Model\api\MarketComments', 'id', 'usersId');
    }

    /**
     * ManyToMany relationship with pivot table with teams table
     */
    public function teams()
    {
        return $this->belongsToMany('App\Model\api\Teams', 'user_has_teams');
    }

    /**
     * ManyToMany relationship with pivot table with teams table
     */
    public function eventsHasUser()
    {
        return $this->belongsToMany('App\Model\api\Events', 'events_has_users', 'usersId', 'eventsId');
    }

    /**
     * Relationship with events table
     */
    public function events() 
    {
        return $this->hasMany('App\Model\api\Events');
    }

    /**
     * Validate password
     *
     * @return bool
     */
    public function validPassword($password) : bool
    {
        return password_verify($password, $this->password);
    }

    /**
     * Generate user token
     *
     * @return void
     */
    public function getToken()
    {
        if (!empty($this->token)) {
            return $this->token;
        }
        
        $token = $this->userToken()->create();
        
        $this->token = $token->token;

        return $this->token;
    }

    /**
     * Set user token
     *
     * @param [type] $token
     * @return void
     */
    public function setToken($token)
    {
        return $this->token = $token;
    }

    /**
     * Email scope query
     *
     * @param [type] $query
     * @param string $email
     * @return void
     */
    public function scopeEmail($query, string $email)
    {
        return $query->where('email', $email);
    }
}
