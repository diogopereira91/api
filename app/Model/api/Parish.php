<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class Parish extends Model
{
    protected $table = "parish";

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `parish` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `description` varchar(100) NOT NULL,
        `countyId` int(11) NOT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_parish_county1_idx` (`countyId`),
        CONSTRAINT `fk_parish_county1` FOREIGN KEY (`countyId`) REFERENCES `county` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

    /**
     * Relationship with address table
     */
    public function address()
    {
        return $this->hasMany('App\Model\api\Address', 'id', 'parishId');
    }

    /**
     * Relatonship with county table
     */
    public function county()
    {
        return $this->belongsTo('App\Model\api\County', 'countyId');
    }

    /**
     * Relationship with address table
     */
    public function fields()
    {
        return $this->hasMany('App\Model\api\Fields');
    }
}
