<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class PasswordResets extends Model
{
    protected $table = 'password_resets';

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `password_resets` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `code` varchar(45) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `used` tinyint(4) NOT NULL,
        `expired` tinyint(4) NOT NULL,
        `usersId` int(11) NOT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_password_resets_users1_idx` (`usersId`),
        CONSTRAINT `fk_password_resets_users1` FOREIGN KEY (`usersId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

    /**
     * Relationship with users table
     */
    public function users()
    {
        return $this->belongsTo('App\Model\api\User', 'usersId');
    }
}
