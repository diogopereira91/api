<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;
use App\Model\Scopes\Active;

class Profile extends Model
{
    protected $DDL = "CREATE TABLE `profile` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `usersId` int(11) NOT NULL,
        `name` varchar(150) NOT NULL,
        `idNumber` varchar(45) NOT NULL,
        `socialNumber` varchar(45) NOT NULL,
        `addressId` int(11) NOT NULL,
        `associationId` int(11) DEFAULT NULL,
        `associationExpiration` timestamp NULL DEFAULT NULL,
        `quotesExpiration` timestamp NULL DEFAULT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_profile_users1_idx` (`usersId`),
        KEY `fk_profile_address1_idx` (`addressId`),
        KEY `fk_profile_association1_idx` (`associationId`),
        CONSTRAINT `fk_profile_address1` FOREIGN KEY (`addressId`) REFERENCES `address` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_profile_association1` FOREIGN KEY (`associationId`) REFERENCES `association` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_profile_users1` FOREIGN KEY (`usersId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

    /**
     * Set table name
     */
    protected $table = 'profile';

    /**
     * Disabling automatic timestamps
     */
    public $timestamps = false;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new Active);
    }

    /**
     * Relationship with users table
     */
    public function users()
    {
        return $this->belongsTo('App\Model\api\User');
    }

    /**
     * Relatonship with address table
     */
    public function address()
    {
        return $this->belongsTo('App\Model\api\Address', 'addressId');
    }

    /**
     * Relatonship with association table
     */
    public function association()
    {
        return $this->belongsTo('App\Model\api\Association', 'associationId');
    }
}
