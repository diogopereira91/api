<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = "news";

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `news` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `title` varchar(150) NOT NULL,
        `body` longtext NOT NULL,
        `picture` varchar(255) DEFAULT NULL,
        `usersId` int(11) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_news_users1_idx` (`usersId`),
        CONSTRAINT `fk_news_users1` FOREIGN KEY (`usersId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";


    /**
     * Relationship with users table
     */
    public function users()
    {
        return $this->belongsTo('App\Model\api\users', 'usersId');
    }

    /**
     * Relationship with comments table
     */
    public function comments()
    {
        return $this->hasMany('App\Model\api\NewsComments', 'id', 'newsId');
    }
}
