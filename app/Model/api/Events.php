<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $table = "events";

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `events` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `usersId` int(11) NOT NULL,
        `description` varchar(150) NOT NULL,
        `rules` longtext NOT NULL,
        `eventDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        `picture` varchar(255) DEFAULT NULL,
        `fieldsId` int(11) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_events_fields1_idx` (`fieldsId`),
        KEY `fk_events_users1_idx` (`usersId`),
        CONSTRAINT `fk_events_fields1` FOREIGN KEY (`fieldsId`) REFERENCES `fields` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_events_users1` FOREIGN KEY (`usersId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

    /**
     * Relatonship with fields table
     */
    public function fields()
    {
        return $this->belongsTo('App\Model\api\Fields', 'fieldsId');
    }

     /**
     * Relatonship with users table
     */
    public function users()
    {
        return $this->belongsTo('App\Model\api\User', 'usersId');
    }

    /**
     * ManyToMany relationship with pivot table with teams table
     */
    public function eventsHasUsers()
    {
        return $this->belongsToMany('App\Model\api\User', 'events_has_users', 'eventsId', 'usersId');
    }

}
