<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `address` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `street` varchar(255) NOT NULL,
        `doorNumber` varchar(45) NOT NULL,
        `zipCode` varchar(45) NOT NULL,
        `parishId` int(11) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_address_parish1_idx` (`parishId`),
        CONSTRAINT `fk_address_parish1` FOREIGN KEY (`parishId`) REFERENCES `parish` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

    /**
     * Relationship with profile table
     */
    public function profile()
    {
        return $this->hasMany('App\Model\api\Profile');
    }

    /**
     * Relatonship with parish table
     */
    public function parish()
    {
        return $this->belongsTo('App\Model\api\Parish', 'parishId');
    }
}
