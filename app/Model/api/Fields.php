<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class Fields extends Model
{
    protected $table = "fields";

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `fields` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `name` varchar(150) NOT NULL,
        `picture` varchar(255) DEFAULT NULL,
        `parishId` int(11) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_fields_parish1_idx` (`parishId`),
        CONSTRAINT `fk_fields_parish1` FOREIGN KEY (`parishId`) REFERENCES `parish` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

    /**
     * Relatonship with parish table
     */
    public function parish()
    {
        return $this->belongsTo('App\Model\api\Parish', 'parishId');
    }

    /**
     * Relationship with events table
     */
    public function events()
    {
        return $this->hasMany('App\Model\api\Events', 'id', 'fieldsId');
    }
}
