<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class Market extends Model
{
    protected $table = "market";

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `market` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `itemTitle` varchar(150) NOT NULL,
        `itemDescription` longtext NOT NULL,
        `itemPrice` varchar(45) NOT NULL,
        `visible` tinyint(4) NOT NULL,
        `itemSoldAt` timestamp NULL DEFAULT NULL,
        `usersId` int(11) NOT NULL,
        `itemTypeId` int(11) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_market_users1_idx` (`usersId`),
        KEY `fk_market_item_type1_idx` (`itemTypeId`),
        CONSTRAINT `fk_market_item_type1` FOREIGN KEY (`itemTypeId`) REFERENCES `item_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_market_users1` FOREIGN KEY (`usersId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";


    /**
     * Relationship with users table
     */
    public function users()
    {
        return $this->belongsTo('App\Model\api\User', 'usersId');
    }

    /**
     * Relationship with item_type table
     */
    public function itemType()
    {
        return $this->belongsTo('App\Model\api\ItemType', 'itemTypeId');
    }

     /**
     * Relationship with market_comments table
     */
    public function comments()
    {
        return $this->hasMany('App\Model\api\MarketComments', 'marketId');
    }
}
