<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    protected $table = "county";

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `county` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `description` varchar(100) NOT NULL,
        `countryId` int(11) NOT NULL,
        PRIMARY KEY (`id`,`description`),
        KEY `fk_county_country1_idx` (`countryId`),
        CONSTRAINT `fk_county_country1` FOREIGN KEY (`countryId`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";


    /**
     * Relationship with parish table
     */
    public function parish()
    {
        return $this->hasMany('App\Model\api\Parish', 'id', 'countyId');
    }

    /**
     * Relatonship with country table
     */
    public function country()
    {
        return $this->belongsTo('App\Model\api\Country', 'countryId');
    }
}
