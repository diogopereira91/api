<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    protected $table = "teams";

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `teams` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `code` varchar(45) NOT NULL,
        `name` varchar(255) NOT NULL,
        `picture` varchar(255) DEFAULT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";


    /**
     * ManyToMany relationship with pivot table with teams table
     */
    public function users()
    {
        return $this->belongsToMany('App\Model\api\User', 'user_has_teams');
    }
}
