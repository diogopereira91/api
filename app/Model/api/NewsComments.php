<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class NewsComments extends Model
{
    protected $table = "comments";

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `comments` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `comment` longtext NOT NULL,
        `newsId` int(11) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        `usersId` int(11) NOT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_comments_news1_idx` (`newsId`),
        KEY `fk_comments_users1_idx` (`usersId`),
        CONSTRAINT `fk_comments_news1` FOREIGN KEY (`newsId`) REFERENCES `news` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_comments_users1` FOREIGN KEY (`usersId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";


    /**
     * Relationship with users table
     */
    public function users()
    {
        return $this->belongsTo('App\Model\api\users', 'usersId');
    }

    /**
     * Relationship with news table
     */
    public function news()
    {
        return $this->belongsTo('App\Model\api\news', 'newsId');
    }
}
