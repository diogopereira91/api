<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    protected $table = 'user_token';

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `user_token` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `token` varchar(255) NOT NULL,
        `usersId` int(11) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_user_token_users1_idx` (`usersId`),
        CONSTRAINT `fk_user_token_users1` FOREIGN KEY (`usersId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";

    /**
     * Set default values
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        if (empty($attributes['token'])) {
            $this->token =\Illuminate\Support\Str::random(200);
        }

        parent::__construct($attributes);
    }

    /**
     * Relationship with user table
     */
    public function users()
    {
        return $this->belongsTo('App\Model\api\User', 'userId');
    }

    /**
     * Token query scope
     *
     * @param [type] $query
     * @param string $token
     * @return void
     */
    public function scopeToken($query, string $token)
    {
        return $query->where('token', $token);
    }
}
