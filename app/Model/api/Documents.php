<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
    protected $table = "documents";

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `documents` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `description` varchar(255) NOT NULL,
        `url` varchar(255) NOT NULL,
        `usersId` int(11) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_documents_users1_idx` (`usersId`),
        CONSTRAINT `fk_documents_users1` FOREIGN KEY (`usersId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";


    /**
     * Relationship with users table
     */
    public function users()
    {
        return $this->belongsTo('App\Model\api\User', 'usersId');
    }
}
