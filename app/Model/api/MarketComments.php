<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class MarketComments extends Model
{
    protected $table = "market_comments";

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `market_comments` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `comment` longtext NOT NULL,
        `marketId` int(11) NOT NULL,
        `usersId` int(11) NOT NULL,
        `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `deleted` timestamp NULL DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `fk_market_comments_market1_idx` (`marketId`),
        KEY `fk_market_comments_users1_idx` (`usersId`),
        CONSTRAINT `fk_market_comments_market1` FOREIGN KEY (`marketId`) REFERENCES `market` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        CONSTRAINT `fk_market_comments_users1` FOREIGN KEY (`usersId`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";


    /**
     * Relationship with users table
     */
    public function users()
    {
        return $this->belongsTo('App\Model\api\User', 'usersId');
    }

    /**
     * Relationship with market table
     */
    public function market()
    {
        return $this->belongsTo('App\Model\api\Market', 'marketId');
    }
}
