<?php

namespace App\Model\api;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = "country";

    public $timestamps = false;

    protected $DDL = "CREATE TABLE `country` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `description` varchar(100) NOT NULL,
        `code` varchar(45) NOT NULL,
        PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";


    /**
     * Relationship with county table
     */
    public function county()
    {
        return $this->hasMany('App\Model\api\County', 'id', 'countryId');
    }
}
