<?php

namespace App\Lib\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Rebing\GraphQL\Error\ValidationError;
use Illuminate\Support\Str;
use App\Facades\GraphQLErrors;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    // /**
    //  * Render an exception into an HTTP response.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  \Exception  $e
    //  * @return \Illuminate\Http\Response
    //  */
    // public function render($request, Exception $e)
    // {
    //     $code = $e->getCode();

    //     if (empty($code)) {
    //         $code = 400;
    //     }

    //     if ($e instanceof ModelNotFoundException) {
    //         $code = 404;
    //     } elseif ($e instanceof HttpException) {
    //         return parent::render($request, $e);
    //     }

    //     $structure             = config('api_structure');
    //     $structure['errors'][] = self::processException($e);

    //     return response()->json($structure, $code);
    // }

    /**
     * Process all exception, including graphql execution
     *
     * @param Exception $e
     * @param boolean $isGraphQL
     * @return array
     */
    private static function processException(Exception $e, bool $isGraphQL = false): array
    {
        $exception = [
            'message' => $e->getMessage(),
        ];

        if ($e instanceof ModelNotFoundException) {
            $exception['message'] = 'no resource was found';
        }

        $previous = $e->getPrevious();
        if (!empty($previous) && !empty($previous->getCode())) {
            $error['code'] = $previous->getCode();
        }

        if ($previous && $previous instanceof ValidationError) {
            $message              = $previous->getValidatorMessages()->all();
            $exception['message'] = $message;

            foreach ($message as $key => $msg) {
                if (Str::startsWith($msg, "GRAPHQL_ERROR:")) {
                    $code = Str::after($msg, "GRAPHQL_ERROR:");
                    if (!empty(GraphQLErrors::$errors[$code])) {
                        $exception['code']     = $code;
                        $exception['mensagem'] = GraphQLErrors::$errors[$code]['mensagem'];
                        $exception['message']  = GraphQLErrors::$errors[$code]['message'];
                        break;
                    }
                }
            }
        }

        if ($previous && $previous instanceof GraphQLErrors) {
            $msg                   = $previous->getMessage();
            $exception['code']     = $msg;
            $exception['mensagem'] = GraphQLErrors::$errors[$msg]['mensagem'];
            $exception['message']  = GraphQLErrors::$errors[$msg]['message'];
        }

        if ($isGraphQL) {
            $trace = self::getGraphQLTrace($e);
        } else {
            $trace = self::getNormalTrace($e);
        }

        $exception = array_merge($exception, $trace);

        return $exception;
    }

    /**
     * Process normal trace. This trace is for all exceptions about PHP, middlware etc.
     *
     * @param Exception $e
     * @return array
     */
    private static function getNormalTrace(Exception $e): array
    {
        $result = [];

        if (env('APP_ENV') != 'production' && !empty($e->getTrace())) {
            $traces = $e->getTrace();
            $file   = $e->getFile();

            $result['class'] = get_class($e);
            $result['trace'] = [
                'file' => $file,
                'line' => $e->getLine(),
            ];
        }

        return $result;
    }

    /**
     * Process GraphQL trace. This trace is for all expcetions on GraphQL execution.
     *
     * @param Exception $e
     * @return array
     */
    private static function getGraphQLTrace(Exception $e): array
    {
        $result = [];

        if (!empty($e->getPrevious())) {
            $previous = $e->getPrevious();

            if (!empty($previous) && !empty($previous->getCode())) {
                $result['code'] = $previous->getCode();
            }

            $traceInfo     = $e->getTrace()[0];
            $argsTraceInfo = $traceInfo['args'][0];

            if (env('APP_ENV') != 'production' && method_exists($argsTraceInfo, 'getTrace')) {
                $moreSpecific = $argsTraceInfo->getTrace()[0];

                $result['trace'] = [
                    'file' => $argsTraceInfo->getFile(),
                    'line' => $argsTraceInfo->getLine(),
                    'class' => $moreSpecific['class'] ?? null,
                    'functionClass' => $moreSpecific['function'] ?? null,
                    'typeException' => get_class($argsTraceInfo),
                ];
            }
        }

        return $result;
    }

    /**
     * Process graphql error
     *
     * @param Exception $e
     * @return array
     */
    public static function graphqlError(Exception $e): array
    {
        return self::processException($e, true);
    }
}
